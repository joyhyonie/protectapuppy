package com.greedy.team1.protectAPuppy;

public class Application {

	public static void main(String[] args) {
		
		/* [ 갱얼쥐를 부탁해 ] */
		/* 학원에서 공부를 열심히 하고 저벅저벅 집으로 걸어가던 길,, 
		 * 당신은 한 털뭉치를 발견하고 너무 가여운 나머지 잠시 보호를 해주기로 합니다. 
		 * 하지만 사실 꽤 까다로운 갱얼쥐였습니다.. 😹
		 * 갱얼쥐가 좋은 주인을 만나기 전까지 열심히 돌봐주도록 해봐요!
		 */

		/* 게임 조건
		 * 미션 5가지를 무사히 수행하여 갱얼쥐의 만족도와 공복감을 최소 5이상으로 확보하여 Happy ending을 맞이하세요 😊
		 * 만족도와 공복감 둘 중 하나라도 0이하이면 갱얼쥐가 가출하니 주의하세요!
		 */
		
		Main start = new Main();
		start.gameStart();
	}
}
