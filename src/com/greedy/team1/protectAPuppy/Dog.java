package com.greedy.team1.protectAPuppy;

public abstract class Dog {
	
	/* 갱얼쥐의 만족도와 공복감 */
	protected static int manjok = 50;
	protected static int hungry = 50;

	public int getManjok() {
		return manjok;
	}

	public void setManjok(int manjok) {
		Dog.manjok = manjok;
	}

	public int getHungry() {
		return hungry;
	}

	public void setHungry(int hungry) {
		Dog.hungry = hungry;
	}

	/* Mission */
	public abstract int feed(int bobGram);
	
	public abstract String play(String toy);
	
	public abstract String givesnack(String snack);
	
}