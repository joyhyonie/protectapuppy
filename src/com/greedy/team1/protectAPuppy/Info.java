package com.greedy.team1.protectAPuppy;

public class Info {

	/* 필드 */
	private String userName;
	private String dogName;
	private String dogType;
	
	/* 생성자 */
	public Info() {}
	
	public Info(String userName, String dogName, String dogType) {
		this.userName = userName;
		this.dogName = dogName;
		this.dogType = dogType;
	}
	
	/* getter & setter */
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDogName() {
		return dogName;
	}

	public void setDogName(String dogName) {
		this.dogName = dogName;
	}

	public String getDogType() {
		return dogType;
	}

	public void setDogType(String dogType) {
		this.dogType = dogType;
	}
}
