package com.greedy.team1.protectAPuppy;

import java.util.Scanner;

public class Main {

	public void gameStart() {
		
		Scanner sc = new Scanner(System.in);
		
		Info info = new Info();
		
		int plus20 = 20;
		int minus20 = -20;
		
		/* 프롤로그 */
		System.out.println("----------------------------------------------------");
		System.out.println("     [갱얼쥐를 부탁해] 시작 전, 당신의 이름을 적어주세요 :)      ");
		System.out.println("----------------------------------------------------");
		System.out.print(" 이름 : ");
		info.setUserName(sc.nextLine());
		System.out.println("----------------------------------------------------");
		System.out.println("하루종일 공부하고 집으로 가던 " + info.getUserName() + ".. 길목 구석에 있는 털뭉치를 발견했다.");
		System.out.print("-------------------------------------------▶ Enter키");
		sc.nextLine();
		System.out.println("??? : 끼잉..");
		System.out.println(info.getUserName() + " : 저게 뭐지? ");
		System.out.print("--------------------------▶ 가까이 가서 보고싶다면 Enter키");
		sc.nextLine();
		System.out.println(" 아기 갱얼쥐 한 마리가 덩그러니 혼자 있어요! \n 마음씨 넉넉한 " + info.getUserName() + "님이 잠시 보살펴 주시기로 마음을 먹었습니다 !");
		System.out.print("-------------------------------------------▶ Enter키");
		sc.nextLine();
		System.out.println("  새로운 가족을 찾아주기 전까지 불러줄 갱얼쥐의 이름을 지어주세요 :)  ");
		System.out.println("----------------------------------------------------");
		System.out.print(" 갱얼쥐 이름 : ");
		info.setDogName(sc.nextLine());
		System.out.println("----------------------------------------------------");
		System.out.println("         자! 이제 " + info.getDogName() + "의 임시 집사가 되어봅시다! ^▽^         ");
		System.out.print("-------------------------------------------▶ Enter키");
		sc.nextLine();
	
		/* 게임 메인 */
		System.out.println("૮ ・ﻌ・ა  " + info.getDogName() + "가 어떤 종으로 보이세요?");
		System.out.print("- 치와와\n- 말티즈\n- 리트리버\n- 시츄\n : ");
		info.setDogType(sc.nextLine());
		System.out.println("----------------------------------------------------");
		
		Dog dogType = null;
		
		switch(info.getDogType()) {
		case "치와와" : 
		dogType = new Chihuahua(info);
		break;
		case "말티즈" : 
		dogType = new Maltese(info);
		break;
		case "리트리버" : 
		dogType = new Retriever(info);
		break;
		case "시츄" : 
		dogType = new ShihTzu(info);
		break;
		}
		
		/* 목욕시키기 */
		System.out.println(info.getDogType() + " " + info.getDogName() + "를 집에 데려와 일단 뽀독뽀독 씻겨줍시다 !");
		System.out.println("[Mission 1] 목욕 순서를 맞춰보세요 ! (ex : 54321)");
		System.out.println("1.말려주기\n2.헹궈주기\n3.뜨신 물로 적셔주기\n4.올인원 샴푸&린스\n5.빗어주기");
		System.out.print(" : ");
		int bath = sc.nextInt();

		if (bath == 34215) {
			System.out.println("----------------------------------------------------");
			System.out.println(" 정답입니다!  ໒・ﻌ・७ 헥헥");
			Dog.manjok += plus20;
			Dog.hungry += minus20;

		} else {
			System.out.println("----------------------------------------------------");
			System.out.println("틀렸어요 ! " + info.getDogName() + "가 불쾌해하네요 ૮ - ﻌ - ა");
			Dog.manjok += minus20;
			Dog.hungry += minus20;
		}	
		System.out.println("----------------------------------------------------");
		System.out.println("현재 만족도 : " + Dog.manjok);
		System.out.println("현재 공복감 : " + Dog.hungry);
		System.out.print("-------------------------------------------▶ Enter키");
		sc.nextLine();
		sc.nextLine();
		
		/* 밥주기*/
		System.out.println(info.getDogName() + "가 많이 굶주렸나봐요.. 밥을 한번 줘볼까요?");
		System.out.print("[Mission 2] 로얄캐닌 몇 그람(g)을 주실건가요? : ");
		int bobGram1 = sc.nextInt();
		dogType.feed(bobGram1);
		System.out.println("----------------------------------------------------");
		System.out.println("현재 만족도 : " + Dog.manjok);
		System.out.println("현재 공복감 : " + Dog.hungry);
		System.out.print("-------------------------------------------▶ Enter키");
		sc.nextLine();
		sc.nextLine();
		
		/* 놀아주기 */
		System.out.println("૮ ˘ﻌ˘ა   " + info.getDogName() + " : " + "멈 멈머 멈멈 (대충 밖에 나가 놀고싶다는 말)");
		System.out.println("이런, 밥을 먹고나니 놀고싶은가봐요\n바깥으로 가볼까요? ");
		System.out.print("-------------------------------------------▶ Enter키");
		sc.nextLine();
		System.out.println("[Mission 3] " + info.getDogName() + "랑 무엇을 가지고 놀아줄까요?");
		System.out.print("- 인형\n- 원반\n- 삑삑이\n- 실뭉타래\n : ");
		String toy = sc.next();
		System.out.println("----------------------------------------------------");
		dogType.play(toy);
		System.out.println("----------------------------------------------------");
		System.out.println("현재 만족도 : " + Dog.manjok);
		System.out.println("현재 공복감 : " + Dog.hungry);
		System.out.print("-------------------------------------------▶ Enter키");
		sc.nextLine();
		sc.nextLine();
		
		/* 간식주기 */
		System.out.println("૮ ・ﻌ・ა   " + info.getDogName() + " : " + "멈멈 (간식)");
		System.out.println("웁스, 집에 있는 간식을 " + info.getDogName() + "가 발견했네요.. 한번 줘봅시다");
		System.out.print("-------------------------------------------▶ Enter키");
		sc.nextLine();
		System.out.println("[Mission 4] " + info.getDogName() + "가 가리키고 있는 간식이 뭘까요?");
		System.out.print("- 고구마\n- 당근\n- 오리육포\n- 닭찌\n : ");
		String snack = sc.next();
		System.out.println("----------------------------------------------------");
		dogType.givesnack(snack);
		System.out.println("----------------------------------------------------");
		System.out.println("현재 만족도 : " + Dog.manjok);
		System.out.println("현재 공복감 : " + Dog.hungry);
		System.out.print("-------------------------------------------▶ Enter키");
		sc.nextLine();
		sc.nextLine();
		
		/* 텔레파시게임하기 */
		System.out.println("૮ ⚆ﻌ⚆ა --------------------☆");
		System.out.println(info.getDogName() + "가 텔레파시를 보냅니다 ~");
		System.out.println("[Mission 5] 1 ~ 5 사이의 수 중, " + info.getDogName() + "가 고른 숫자를 맞춰주세요 !");
		System.out.print(" : ");
			int a = sc.nextInt(); 
			int num1 = (int) ((Math.random() * 5)+ 1);
			if(a<6 && a>0 && a == num1 ) {
				System.out.println("----------------------------------------------------");
				System.out.println("우와 !! " + info.getDogName() + "와 텔레파시가 통했어요! ૮ ⚆ﻌ⚆ა");
				Dog.manjok += plus20;
						}
			else {
				System.out.println("----------------------------------------------------");
				System.out.println(info.getDogName() + "와 텔레파시가 통하지 않았습니다.. 흥 ૮ ˘ﻌ˘ა");
				Dog.manjok += minus20;
			}
			System.out.println("----------------------------------------------------");
			System.out.println("현재 만족도 : " + Dog.manjok);
			System.out.println("현재 공복감 : " + Dog.hungry);
			System.out.print("-------------------------------------------▶ Enter키");
			sc.nextLine();
			sc.nextLine();
			
			/* 엔딩 */
			if(Dog.manjok > 0 && Dog.hungry > 0) {
				System.out.println(info.getDogName() + "는 행복한 갱얼쥐 훈장을 받고 좋은 주인의 품으로 갔어요 ♥ ૮ ♡ﻌ♡ა  멈!");
				System.out.println("-------------------HAPPY ENDING---------------------");
			} else if(Dog.manjok <= 0){
				System.out.println(info.getDogName() + "가 " + info.getUserName() + "님의 보살핌에 불만을 가득 품고 가출했습니다..Bye..");	
				System.out.println("--------------------BAD ENDING----------------------");
			} else if(Dog.hungry <= 0) {
				System.out.println(info.getDogName() + "는 너무 배고파서 " + info.getUserName() + "님의 집을 탈출했습니다..Bye..");	
				System.out.println("--------------------BAD ENDING----------------------");
			}
		}
}
