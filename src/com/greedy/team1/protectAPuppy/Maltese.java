package com.greedy.team1.protectAPuppy;

public class Maltese extends Dog {

private Info info;
	
	int plus20 = 20;
	int plus10 = 10;
	int plus5 = 5;
	int minus20 = -20;
	int minus10 = -10;
	int minus5 = -5;
	
	public Maltese(Info info) {
		this.info = info;
	}
	
	public int getMan() {
		return manjok;
	}

	public void setMan(int man) {
		Dog.manjok = manjok;
	}

	public int getHungry() {
		return hungry;
	}

	public void setHungry(int hungry) {
		Dog.hungry = hungry;
	}
	
	@Override
	public int feed(int bobGram){
		
		if(bobGram > 60 || bobGram < 40) {
			System.out.println("----------------------------------------------------");
			System.out.println("양이 너무 적거나 많아 " + info.getDogName() + "가 분노합니다 ૮ - ﻌ - ა ");
				Dog.manjok += minus20;
				Dog.hungry += minus10;
		} else if(bobGram <= 60 || bobGram >= 40) {
			System.out.println("----------------------------------------------------");
			System.out.println(info.getDogName() + "가 만족합니다 ૮ ♡ﻌ♡ა");
				Dog.manjok += plus10;
				Dog.hungry += plus10;
		} 
		return bobGram;
	}
	
	@Override
	public String play(String toy) {

		switch(toy) {
		case "실뭉타래" : 
			System.out.println(info.getDogName() + "가 실뭉타래를 엄청 좋아하네요 !  ૮ ♡ﻌ♡ა ");
			Dog.manjok += plus20;
			Dog.hungry += minus5;
			break;
		case "인형" : 
			System.out.println(info.getDogName() + "가 인형에 관심이 없네요  ૮ - ﻌ - ა ");
			Dog.manjok += minus10;
			Dog.hungry += minus10;
			break;
		case "원반" :
			System.out.println(info.getDogName() + "가 원반을 좋아하네요 ~  ૮ ⚆ﻌ⚆ა ");
			Dog.manjok += plus10;
			Dog.hungry += minus5;
			break;
		case "삑삑이" :
			System.out.println(info.getDogName() + "가 삑삑이를 싫어..하네요  ૮ ˘ﻌ˘ა "); 
			Dog.manjok += minus20;
			Dog.hungry += minus20;
			break;
			default : System.out.println("[ERROR]" + toy + " 라는 장난감은 없습니다! 돌아가세요!");
			Dog.manjok += minus20;
			Dog.hungry += minus20;
		}
		return "";
	} 
	
	@Override
	public String givesnack(String snack) {

		switch(snack) {
		case "닭찌" : 
			System.out.println(info.getDogName() + "가 닭찌에 환장합니다 !  ૮ ♡ﻌ♡ა ");
			Dog.manjok += plus20;
			Dog.hungry += plus20;
			break;
		case "고구마" : 
			System.out.println(info.getDogName() + "가 고구마를 먹고 뱉습니다.  ૮ - ﻌ - ა ");
			Dog.manjok += minus10;
			Dog.hungry += minus20;
			break;
		case "당근" : 
			System.out.println(info.getDogName() + "가 당근을 외면합니다  ૮ ˘ﻌ˘ა ");
			Dog.manjok += minus20;
			Dog.hungry += minus20;
			break;
		case "오리육포" : 
			System.out.println(info.getDogName() + "가 오리육포를 좋아하네요 ~  ૮ ⚆ﻌ⚆ა ");
			Dog.manjok += plus10;
			Dog.hungry += plus10;
			break;
		default : System.out.println("[ERROR]" + snack + " 라는 간식은 없습니다! 돌아가세요!");
		Dog.manjok += minus20;
		Dog.hungry += minus20;
		}
		return "";
	}
}
